//
//  main.m
//
//  Created by xiayang on 13-2-13.
//  Copyright (c) 2013年 eoe.cn. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSString *str1 = @"www.eoe.cn";
        
        NSRange r = [str1 rangeOfString:@"eoe"];
        if (r.location!=NSNotFound) {
            NSLog(@"found at location = %d,length = %d",r.location,r.length);
        }
        else
        {
            NSLog(@"not found");
        }
          
    }
    return 0;
}      